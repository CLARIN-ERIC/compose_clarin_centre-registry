#!/usr/bin/env bash
set -e

SCRIPTS_PATH=$(realpath "$(dirname "${BASH_SOURCE[0]}")")
# shellcheck disable=SC1091
source "${SCRIPTS_PATH}/common-lib.sh"

main ( ) {
    VERBOSE=0
    HELP=0
    ERROR=0

    while [ $# -gt 0 ]
        do
        key="$1"
        case $key in
            "")
                ;;
            -h|--help)
                HELP=1
                ;;
            -v|--verbose)
                VERBOSE=1
                ;;
            *)
                error "Unkown option: $key"
                HELP=1
                ERROR=1
                ;;
        esac
        shift
    done

    if [ "${VERBOSE}" -eq 1 ]; then
        set -x
    fi

    if [ "${HELP}" -eq 1 ]; then
        echo "Backup database to ../backups"
        echo "Usage: control.sh [<name>|-s] backup [-hv]"
        echo ""
        echo "  -v, --verbose             Run in verbose mode"
        echo ""
        echo "  -h, --help                Show help"
        echo ""
        if [ "${ERROR}" -eq 0 ]; then
            exit 0
        else
            exit 1
        fi
    fi
    run_backup
}

run_backup ( ) {
    container_is_healthy "${COMPOSE_DIR}" "${UWSGI_HOST}" || \
        (fatal "Service \"$UWSGI_HOST\" is not running")
    echo "Backing up database... "
    APP_RUN_CONTAINER_NAME=$(get_container_name "${COMPOSE_DIR}" "${UWSGI_HOST}")
    TIMESTAMP=${BACKUPS_TIMESTAMP:-${DATE}} # Use BACKUPS_TIMESTAMP when called from control.sh
    BACKUP_FILE="$(get_abs_filename "${COMPOSE_DIR}"/../../backups/database_"${TIMESTAMP}".sqlite)"
    docker cp "${APP_RUN_CONTAINER_NAME}":/srv/database/database.sqlite "${BACKUP_FILE}"
    echo "Database backup saved to: ${BACKUP_FILE}"
    exit 0
}

main "$@"
