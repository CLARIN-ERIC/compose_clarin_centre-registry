#!/usr/bin/env bash
set -e

SCRIPTS_PATH=$(realpath "$(dirname "${BASH_SOURCE[0]}")")
# shellcheck disable=SC1091
source "${SCRIPTS_PATH}/common-lib.sh"

main ( ) {
    VERBOSE=0
    HELP=0
    ERROR=0

    while [ $# -gt 0 ]
        do
        key="$1"
        case $key in
            "")
                ;;
            -h|--help)
                HELP=1
                ;;
            -v|--verbose)
                VERBOSE=1
                ;;
            *)
                ARGS_ARRAY+=("$key")
                ;;
        esac
        shift
    done

    if [ "${VERBOSE}" -eq 1 ]; then
        set -x
    fi

    if [ -n "${ARGS_ARRAY[0]}" ]; then
        case ${ARGS_ARRAY[0]} in
            latest|*)
                if [ -z "${ARGS_ARRAY[1]}" ] && [ -f "${ARGS_ARRAY[0]}" ] || is_timestamp_valid "${ARGS_ARRAY[0]}"; then
                    run_restore "${ARGS_ARRAY[0]}"
                else
                    error "Invalid parameter: ${ARGS_ARRAY[0]}"
                    echo ""
                    HELP=1
                    ERROR=1
                fi
                ;;
        esac
    fi

    if [ "${HELP}" -eq 1 ]; then
        echo "Restore database from specified <timestamp>, or if \"latest\" is specified, use the most recent .sqlite file available in ../backups"
        echo ""
        echo "Usage: control.sh [<name>|-s] restore [-h|-v] [timestamp|filename|'latest']"
        echo ""
        echo "  Commands:"
        echo "    latest                  Restore the latest available backups."
        echo "    <filename>              Restore the backup identified by the file name <filename>."
        echo "    <timestamp>             Restore the backup identified by <timestamp> as displayed by \`restore.sh ls\`"
        echo ""
        echo "  Optional flags:"
        echo "    -v, --verbose           Run in verbose mode"
        echo "    -h, --help              Show help"
        echo ""
        if [ "${ERROR}" -eq 0 ]; then
            exit 0
        else
            exit 1
        fi
    fi
}

run_restore ( ) {
    container_is_healthy "${COMPOSE_DIR}" "${UWSGI_HOST}" || \
        (echo "Service \"$UWSGI_HOST\" is not running" && exit 1)

    # File can be supplied either via RESTORE_FILE (prioritary) env variable or as the first parameter of this script
    RESTORE_DB_FILE="${RESTORE_FILE:-$1}"
    if [ -f "${RESTORE_DB_FILE}" ]; then
        echo "Using database file \"${RESTORE_DB_FILE}\""
        RESTORE_DB_FILE=$(get_abs_filename "${RESTORE_DB_FILE}")
    elif [ "${RESTORE_DB_FILE}" == "latest" ]; then
        echo "Using latest database file"
        RESTORE_DB_FILE="$(get_abs_filename "$(unset -v latest; for file in "${COMPOSE_DIR}"/../../backups/*.sqlite; do [ ! -L "$file" ] && [[ "$file" -nt "$latest" ]] &&  latest=$file; done; echo  "$latest")")"
    elif is_timestamp_valid "${RESTORE_DB_FILE}"; then
        echo "Using database file with timestamp ${1}"
        RESTORE_DB_FILE="$(get_abs_filename "${COMPOSE_DIR}"/../../backups/database_"${1}".sqlite)"
    else
        echo "Cannot find database file: ${RESTORE_DB_FILE}"
        exit 1
    fi

    echo "Restoring database file: ${RESTORE_DB_FILE} ..."

    APP_RUN_CONTAINER_NAME=$(get_container_name "${COMPOSE_DIR}" "${UWSGI_HOST}")
    docker cp -a "${RESTORE_DB_FILE}" "${APP_RUN_CONTAINER_NAME}":/srv/database/database.sqlite
    docker exec "${APP_RUN_CONTAINER_NAME}" bash -c "chown -R uwsgi:uwsgi /srv/database"
    docker exec "${APP_RUN_CONTAINER_NAME}" bash -c "chmod 0600 /srv/database/database.sqlite"
    set_readonly "${COMPOSE_DIR}" "${UWSGI_HOST}" "${READONLY}"
    echo "Database restored."
    exit 0
}

main "$@"; exit
