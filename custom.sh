#!/usr/bin/env bash
set -e

SCRIPTS_PATH=$(realpath "$(dirname "${BASH_SOURCE[0]}")")
# shellcheck disable=SC1091
source "${SCRIPTS_PATH}/common-lib.sh"

main () {

    VERBOSE=0
    HELP=0
    ERROR=0

    while [ $# -gt 0 ]
        do
        key="$1"
        case $key in
            "")
                ;;
            -h|--help)
                HELP=1
                ;;
            -v|--verbose)
                VERBOSE=1
                ;;
            *)
                ARGS_ARRAY+=("$key")
                ;;
        esac
        shift
    done

    if [ "${VERBOSE}" -eq 1 ]; then
        set -x
    fi

    if [ -n "${ARGS_ARRAY[0]}" ]; then
        case ${ARGS_ARRAY[0]} in
            pull-state)
                SCP_ADDR=${ARGS_ARRAY[1]}
                if [ -z "$SCP_ADDR" ] || ! grep -q ':' <<<"$SCP_ADDR"; then
                    error "Please provide an scp location <server>:<path>"
                    HELP=1
                fi
                restore_from_remote "$SCP_ADDR"
                ;;
            set-readonly)
                STATE=${ARGS_ARRAY[1]}
                if [ -z "${STATE}" ] || ! grep -q -E '0|1' <<< "${STATE}"; then
                    error "Please provide a readonly state: 0|1"
                    HELP=1
                else
                    info "Setting instance to $(if [ "${STATE}" -eq 1 ]; then echo \"read only\"; else echo \"read/write\";fi) mode"
                    set_readonly "${COMPOSE_DIR}" "${UWSGI_HOST}" "${STATE}"
                    exit 0
                fi
                ;;
            apply-update)
                    # The application code is store in a docker volume
                    remove_volumes
                    exit 0
                ;;
            *)
                fatal "Subcommand not available in $(basename "${0}") script!"
                ;;
        esac
    fi

    if [ "${HELP}" -eq 1 ]; then
        echo "    pull-state <scp>              Pull to this instance the database from the remote instance specified by"
        echo "                                    scp location <scp> in the form <server>:<path>."
        echo "    set-readonly <state:0|1>      Temporarily set database to read-only (\"set-readonly 1\") or read/write"
        echo "                                    (\"set-readonly 0\"). Upon restart, the read-only state will be reset"
        echo "                                    based on the \$READONLY environment variable from the .env file."
        echo ""
        if [ "${ERROR}" -eq 0 ]; then
            exit 0
        else
            exit 1
        fi
    fi
}

restore_from_remote ( ) {
    info "Pulling database from running instance on ${1}"
    IFS=':' read -r -a arrIN <<< "$1"
    SCP_SERVER="${arrIN[0]}"
    SCP_PATH="${arrIN[1]}"
    TIMESTAMP="${BACKUPS_TIMESTAMP:-$(date +%Y%m%dT%H%M%S)}" # Use BACKUPS_TIMESTAMP when called from control.sh
    LOCAL_BACKUP=${COMPOSE_DIR}/../../backups/remote-database_${TIMESTAMP}.sqlite
    BACKUP_FILE=${SCP_PATH}/sync_backups/remote-database_${TIMESTAMP}.sqlite

    # Generate temporary backups remotely
    info "Creating a fresh remote backup..."
    # Set REMOTE_PREPARER_HOST based on variable from .env
    if grep -q -E '^REMOTE_PREPARER_HOST=' "${COMPOSE_DIR}"/.env; then
        eval "$(grep "REMOTE_PREPARER_HOST=" "${COMPOSE_DIR}"/.env)"
    else
        REMOTE_PREPARER_HOST="$UWSGI_HOST"
    fi
    if readlink "$0"  >/dev/null 2>&1; then
        REMOTE_COMPOSE_DIR=$COMPOSE_DIR
    else
        REMOTE_COMPOSE_DIR=$(basename "$(cd "$(dirname "${BASH_SOURCE[0]}")" && echo "$PWD")")/$(basename "${COMPOSE_DIR}")
    fi
    SSH_COMMAND="bash $(if [ ${VERBOSE} -eq 1 ]; then echo \"-x\";fi) -c \"
        mkdir -p ${SCP_PATH}/sync_backups/
        cd ${SCP_PATH}/${REMOTE_COMPOSE_DIR}
        REMOTE_CONTAINER_NAME=\\\$(docker-compose images ${REMOTE_PREPARER_HOST} | grep ${REMOTE_PREPARER_HOST} | awk '{ print \\\$1 }')
        if ! docker cp \\\${REMOTE_CONTAINER_NAME}:/srv/database/database.sqlite ${BACKUP_FILE}; then
            docker cp \\\${REMOTE_CONTAINER_NAME}:/srv/database.sqlite ${BACKUP_FILE}
        fi
        exit
        \"
    "
    ssh "$SCP_SERVER" -tt "$SSH_COMMAND"

    info "Remote backup successfuly created. Downloading it..."
    (cd "${COMPOSE_DIR}"/../..
    scp "${SCP_SERVER}:${BACKUP_FILE}" backups/

    SSH_COMMAND="bash -c \"rm -rf ${SCP_PATH}/sync_backups; exit;\""
    ssh "$SCP_SERVER" -tt "$SSH_COMMAND")

    RESTORE_DB_FILE=$(get_abs_filename "${LOCAL_BACKUP}")
    "${SCRIPTS_PATH}"/restore.sh "${RESTORE_DB_FILE}"
    exit 0
}

remove_volumes ( ) {
    (cd "$COMPOSE_DIR" && docker-compose down --volumes)
}

main "$@"; exit

