= Compose configuration for the CLARIN Centre Registry
:caution-caption: ☡ CAUTION
:important-caption: ❗ IMPORTANT
:note-caption: 🛈 NOTE
:sectanchors:
:sectlinks:
:sectnumlevels: 6
:sectnums:
:source-highlighter: pygments
:tip-caption: 💡 TIP
:toc-placement: preamble
:toc:
:warning-caption: ⚠ WARNING

Docker compose deployment setup for the CLARIN Centre Registry.

== Deploy

Checkout or download this repository.

== Run

The project is meant to be controlled by the https://gitlab.com/CLARIN-ERIC/control-script[CLARIN crontrol script].

You can either use a global instance of the control script in multi-project mode, or deploy the control script inside this project directory (or parent) running it in single-project mode (with the `-s` parameter).

For convenience the CLARIN crontrol script is included in this repository as a submodule and can be used single-project mode to control the application.

Checkout the `control-script` submodule and from the project root run:
```
./control.sh -s start
```

By default the application's UI runs at:

https://localhost:44335/

Other options:
```
Usage: control.sh [-h|-v] -s <command>

  Required arguments
    <command>                     One of the available commands listed in the "Commands" section below.

  Optional flags:
    -v, --verbose                 Run in verbose mode
    -h, --help                    Show help
    -s, --single                  Run in single project mode.

  Commands:
    start                         Start (docker-compose up -d)
    stop                          Stop (docker-compose down)
    restart                       Restart, runs stop and start
    status                        Check status of all services
    logs                          Tail logs of all services
    pull                          Pull service images
    exec <name> <command>         Exec the specified <command> in the service with <name>

    backup                        Create a backup
    restore                       Restore a backup
    restore ls                    List available backups for restore

  Custom commands:
    pull-state <scp>              Pull to this instance the database from the remote instance specified by 
                                    scp location <scp> in the form <server>:<path>.
    set-readonly <state:0|1>      Temporarily set database to read-only ("set-readonly 1") or read/write
                                    ("set-readonly 0"). Upon restart, the read-only state will be reset
                                    based on the $READONLY environment variable from the .env file.
```

=== Backup

From the project root run:
```
./control.sh -s backup
```

This will create a database backup and save it to `<project_home>/../backups/database_<date&time>.sqlite`

=== Restore

From the project root run:
```
./control.sh -s restore latest
```

This will restore the most recent database backup from `<project_home>/../backups/` into the running instance.

To specify a specifc dabase backup file to restore, run:
```
./control.sh -s restore <timestamp>|<filepath>
```
With <timestamp> as displayed by `control.sh -s restore ls`

=== Synchronize with remote instance

The `control.sh` script implements a routine wich allows pulling the database state from a remote instance to the local one.

From the project root run:
```
./control.sh -s pull-state user@remote.vm.net:/path/to/compose/project/on/remote
```

To perform the pull, this call uses a SSH connection to the remote host. Consequently the local host must be able to connect to the specified remote via SSH.

Automatic pulls, can done by setting up SSH key-based authentication from the local to the remote, and creating a passwordless key for the authentication (key security must then be implemented via file system permissions). Finally a cron job can be created to perfom the task periodicaly.

To specify the right key for the connection, create an entry on `~/.ssh/config`. This also allows for reducing the complexity of the `control.sh` parameters. For example:

For the remote host URL: `remote.vm.net`, using the user: `crsyncuser` and the key: `sync_ssh_key.key`. Create the following entry in `~/.ssh/config`:
```
Host synchost
    HostName remote.vm.net
    User crsyncuser
    IdentityFile ~/.ssh/sync_ssh_key.key
```

Then place the key in `~/.ssh/sync_ssh_key.key` and set the key permissions to `400`.

The remote database can now be pulled like:
```
./control.sh -s pull-state synchost:/path/to/compose/project/on/remote
```


== Configuration variables
Configuration options can be overriden by supplying an `.env` file in the parent directory of this project:
[source,sh]
----
_SERVERNAME=centres.clarin.eu
_DJANGO_SETTINGS_MODULE=centre_registry_project.settings
_DJANGO_DEBUG=False
_DJANGO_DB_SECRET_KEY=test_key
_DJANGO_PIWIK_WEBSITE_ID=11
READONLY=0
REMOTE_PREPARER_HOST=optional_different_preparer_name_on_remote #optional
_SMTPHOST=mail
----

Note that the `READONLY` and `REMOTE_PREPARER_HOST` variables are only used by the `control.sh` script and so they are not passed to the running containers.

=== `READONLY`
This variable allows setting the Centre Registry database to read-only. The `READONLY` value is red and applied by the `control.sh` script every time the application is started, as well as after a database backup is restored.

It is possible to temporarely change the database read-only status while live. For this run:
```
./control.sh -s set-readonly 1|0
```

=== `REMOTE_PREPARER_HOST`
This variable is optional and allows for specifying a different container name for the container provisioning the Centre Registry on a remote host. Used for pulling its database.

When this variable is not specified, the pull process will try to use the same container name on the remote as the local one.

=== `_SMTPHOST`
If you have a docker container running an email relay in the same host, specify it via this variable. This will allow all Centre Registry containers to send emails.

== Test locally

To test the full setup locally run:
```
./compose-test.sh --local
```

If everything goes well, the script should exit with a return code of 0.

== Components

The docker compose file deploys a number of compoments, described in this section, as a functional service.

=== Application data volumes

Centre Registry application image. https://gitlab.com/CLARIN-ERIC/docker-alpine-centre_registry[Link to image project].

When running this image, the new container will generate a new configuration file for the Centre Registry based on `Centre-Registry-config/sentings.py.tmpl` and the supplied configuration values in `.env`. After this, the container will run the Python tests of the Centre Registry's UI and API and exit shortly after that.

This container provisions three volumes:

. `/srv/Centre-Registry`:
 Contains the CLARIN Centre-Registry Python application. This volume is shared at runtime with the uWSGI container. https://gitlab.com/CLARIN-ERIC/docker-alpine-supervisor-uwsgi/[Link to uWSGI image project]

. `/srv/Centre-Registry/assets`:
 Contains the application static files. This volume is shared at runtime with the NGINX container. https://gitlab.com/CLARIN-ERIC/docker-alpine-nginx[Link to NGINX image project]

. `/srv/Centre-Registry/database`:
 Contains the application database. This volume is also shared at runtime with the uWSGI container. https://gitlab.com/CLARIN-ERIC/docker-alpine-supervisor-uwsgi/[Link to uWSGI image project]

The database and generated configuration files are persisted in the created volumes and will only be recreated if deleted.

The `backups` directory of the host OS is mounted inside this container on `/centre_registry_config.d/Centre-Registry-database/`. When there is no database file at startup, the container will restore the most recent database file available in `/centre_registry_config.d/Centre-Registry-database/`.

=== NGINX server

Generic image. https://gitlab.com/CLARIN-ERIC/docker-alpine-nginx[Link to image project].

The NGINX server handles all http traffic.  It proxies all CGI requests to the uWSGI container and directly serves the Centre Registry static resources.

The `nginx-config/default.conf` is supplied to this container at `/nginx_conf.d/default.conf` and configures NGINX's uWSGI proxy and the location of the static files.

The static files are supplied by the `/srv/Centre-Registry/assets` volume, provisioned by the `Application data` container and mounted read-only on this component's container.

=== uWSGI server

Generic image. https://gitlab.com/CLARIN-ERIC/docker-alpine-supervisor-uwsgi[Link to image project].

The uWSGI server handles the dynamic requests of the Centre Registry.

The `uwsgi-config\centre-registry.ini` is supplied to this container at `/uwsgi_conf.d/centre-registry.ini`. This configures the uWSGI server to run the Python application from the `/srv/Centre-Registry` volume, which is pre-configured to use the database persisted by the `srv/Centre-Registry/database` volume. Both volumes are provisioned by the `Application data` container.

The `/srv/Centre-Registry` volume is mounted read-only on this component's container.

The `/srv/Centre-Registry/database` volume is mounted read-write on this component's container.

== Server deployments

This compose project currently deploys two live versions of the CLARIN Centre Registry each of them deployed on a trans-datacenter redundant setup:
- https://centres.clarin.eu/
- https://centres-staging.clarin.eu/
