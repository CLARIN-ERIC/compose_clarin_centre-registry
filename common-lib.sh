#!/usr/bin/env bash
# shellcheck disable=SC2034

HTTPSERVER_HOST=nginx
UWSGI_HOST=uwsgi
COMPOSE_DIR="clarin"
if readlink "$0"  >/dev/null 2>&1; then
    COMPOSE_DIR=$(dirname "$(readlink "$0")")/$COMPOSE_DIR
else
    COMPOSE_DIR=$(dirname "${BASH_SOURCE[0]}")/$COMPOSE_DIR
fi
# Set READONLY from .env file 
eval "$(grep -E "^READONLY=" "${COMPOSE_DIR}"/.env)"
DATE=$(date +%Y%m%dT%H%M)

container_is_running ( ) {
    if ! (cd "$1" && docker-compose ps "$2" | grep -q "Up "); then
        return 1
    else
        return 0
    fi
}

container_is_healthy ( ) {
    # $1 : Docker compose file location
    # $2 : Service name
    if ! (cd "$1" && docker-compose ps "$2" | grep -q "(healthy)"); then
        return 1
    else
        return 0
    fi
}

get_container_name ( ) {
    # $1 : Docker compose file location
    # $2 : Service name
    (cd "$1" || exit 1
    docker-compose images "$2" | grep "$2" | awk '{ print $1 }')
}

get_abs_filename( ) {
    # $1 : relative filename
    echo "$(cd "$(dirname "$1")" && pwd)/$(basename "$1")"
}

set_readonly ( ) {
    # $1 : Docker compose file location
    # $2 : Service name
    # $3 : Read only: 0|1
    APP_RUN_CONTAINER_NAME=$(get_container_name "$1" "$2")
    if [ "${3}" -eq 1 ]; then
        info "Setting instance to READONLY mode!"
        docker exec "${APP_RUN_CONTAINER_NAME}" bash -c "chmod 400 /srv/database/database.sqlite"
    else
        docker exec "${APP_RUN_CONTAINER_NAME}" bash -c "chmod 600 /srv/database/database.sqlite"
    fi
}

is_timestamp_valid() {
    # Validate timestamp
    if [ -z "${1}" ]; then
        error "Backup timestamp must be supplied"
        return 1
    fi
    if [[ ! "${1}" =~ ^([0-9]{8}T[0-9]{6}|latest)$ ]]; then
        error "Invalid timestamp: \"${1}\""
        return 1
    fi
    return 0
}

info() {
    log "INFO" "${1}"
}

warn() {
    log "WARN" "${1}"
}

error() {
    log "ERROR" "${1}"
}

fatal() {
    log "FATAL" "${1}"
    exit 1
}

log() {
    TIMESTAMP=$(date '+%Y-%m-%d %H:%M:%S')
    LVL=$1
    MSG=$2
    echo "[${TIMESTAMP}] [${LVL}] ${MSG}"
}
